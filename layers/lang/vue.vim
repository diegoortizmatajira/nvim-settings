call setup#registerlayer('lang#vue')

function! lang#vue#plugins() abort

  call setup#plugin('posva/vim-vue')
  call setup#plugin('leafOfTree/vim-vue-plugin')

endfunction

function! lang#vue#settings() abort
  " Settings

  let g:vue_disable_pre_processors=1
  let g:vim_vue_plugin_load_full_syntax = 1

  " Keybindings

endfunction


"########################################################################
" Support Setup
"########################################################################
function s:support()

endfunction
