call setup#registerlayer('dev')

" Command definition and default implementations
let g:spaced_dev_settings = {
            \"Commands" : {
            \  "test": {},
            \  "testsuite": {},
            \  "testfile": {},
            \  "testfunc": {},
            \  "testlast": {},
            \  "build": {},
            \  "coverage": {},
            \  "debugstart": {},
            \  "debugbreakpoint": {},
            \  "alternate": {},
            \  "findusages": { '*': ":call CocActionAsync('jumpReferences')" },
            \  "findimplementations": { '*': ":call CocActionAsync('jumpImplementation')" },
            \  "refactor": { '*': ":call CocActionAsync('refactor')" },
            \  "previewdefinitions": {},
            \  "previewimplementations": {},
            \  "typelookup": {},
            \  "viewdocumentation": {},
            \  "findsymbol": {},
            \  "fiximports": {},
            \  "signaturehelp": {},
            \  "globalcodecheck": {},
            \  "viewcodeactions": {},
            \  "codeformat": { '*': ":Autoformat<CR>" },
            \  "rename": { '*': ":call CocActionAsync('rename')" },
            \  "definition": { '*': ":call CocActionAsync('jumpDefinition')" },
            \  "typedefinition": { '*': ":call CocActionAsync('jumpTypeDefinition')" },
            \  "quickfix": { '*': ":call CocActionAsync('doQuickfix')" },
            \  },
            \}

function! dev#plugins() abort

    call setup#plugin('neoclide/coc.nvim', {'branch': 'release'})
    call setup#plugin('honza/vim-snippets')
    " call setup#plugin('sheerun/vim-polyglot')
    call setup#plugin('dense-analysis/ale')
    call setup#plugin('Chiel92/vim-autoformat')
    call setup#plugin('puremourning/vimspector')

endfunction

function! dev#settings() abort
    " Settings
    call s:cocSetup()
    call s:aleSetup()

    " Keybindings

    " Use `[c` and `]c` to navigate diagnostics
    nmap <silent> [c <Plug>(coc-diagnostic-prev)
    nmap <silent> ]c <Plug>(coc-diagnostic-next)

    " Use K to show documentation in preview window
    nnoremap <silent> K :call dev#show_documentation()<CR>

    " Coc Snippets

    " Use <C-l> for trigger snippet expand.
    imap <C-l> <Plug>(coc-snippets-expand)

    " Use <C-j> for select text for visual placeholder of snippet.
    vmap <C-j> <Plug>(coc-snippets-select)

    " Use <C-j> for jump to next placeholder, it's default of coc.nvim
    let g:coc_snippet_next = '<tab>'

    " Use <C-k> for jump to previous placeholder, it's default of coc.nvim
    let g:coc_snippet_prev = '<S-tab>'

    " Use <C-j> for both expand and jump (make expand higher priority.)
    imap <C-j> <Plug>(coc-snippets-expand-jump)

    " Use <leader>x for convert visual selected code to snippet
    xmap <leader>x  <Plug>(coc-convert-snippet)


    call mapping#space("nmap", ['b', 'f'], ':Autoformat<CR>', 'Format Buffer')

    call mapping#space("nmap", ['v', 'd'], ':<C-u>CocList diagnostics<CR>', 'Diagnostics')
    call mapping#space("nmap", ['v', 'o'], ':<C-u>CocList outline<CR>', 'Outline')
    call mapping#space("nmap", ['v', 's'], ':<C-u>CocList -I symbols<CR>', 'Symbols')


    call mapping#space("nmap", ['d', 'h'], ':call dev#show_documentation()<CR>', 'Hint Documentation')
    call mapping#space('nmap', ['d', 'b'], ':call dev#executerunner("build")<CR>', 'Build')
    call mapping#space('nmap', ['d', 'a'], ':call dev#executerunner("alternate")<CR>', 'Toggle to alterate file')
    call mapping#space('nmap', ['d', 'u'], ':call dev#executerunner("findusages")<CR>', 'Find usages')
    call mapping#space('nmap', ['d', 'i'], ':call dev#executerunner("findimplementations")<CR>', 'Find implementations')
    call mapping#space('nmap', ['d', 'l'], ':call dev#executerunner("typelookup")<CR>', 'Type lookup')
    call mapping#space('nmap', ['d', 'o'], ':call dev#executerunner("viewdocumentation")<CR>', 'Navigate to documentation')
    call mapping#space('nmap', ['d', 's'], ':call dev#executerunner("findsymbol")<CR>', 'Find symbols')
    call mapping#space('nmap', ['d', 'h'], ':call dev#executerunner("signaturehelp")<CR>', 'Signature help')
    call mapping#space('nmap', ['d', 'c'], ':call dev#executerunner("globalcodecheck")<CR>', 'Global code check')
    call mapping#space('nmap', ['d', 'r'], ':call dev#executerunner("rename")<CR>', 'Rename')
    call mapping#space('nmap', ['d', 'R'], ':call dev#executerunner("refactor")<CR>', 'Refactor')
    call mapping#space("nmap", ['d', 'd'], ':call dev#executerunner("definition")<CR>', 'Go to definition')
    call mapping#space("nmap", ['d', 'D'], ':vsp<CR>:call dev#executerunner("definition")<CR>', 'Go to definition (v-split)')

    call mapping#spacemenu(['d', 'p'], 'Preview')
    call mapping#space('nmap', ['d', 'p', 'd'], ':call dev#executerunner("previewdefinitions")<CR>', 'Preview definitions')
    call mapping#space('nmap', ['d', 'p', 'i'], ':call dev#executerunner("previewimplementations")<CR>', 'Preview implementations')

    call mapping#space('nmap', ['D', 's'], ':call dev#executerunner("debugstart")<CR>', 'Debug start')
    call mapping#space('nmap', ['D', 'b'], ':call dev#executerunner("debugbreakpoint")<CR>', 'Toggle breakpoint')

    call mapping#space('nmap', ['t', '.'], ':call dev#executerunner("test")<CR>', 'Run project tests')
    call mapping#space('nmap', ['t', 's'], ':call dev#executerunner("testsuite")<CR>', 'Run suite tests')
    call mapping#space('nmap', ['t', 'f'], ':call dev#executerunner("testfile")<CR>', 'Run file tests')
    call mapping#space('nmap', ['t', 'n'], ':call dev#executerunner("testfunc")<CR>', 'Run nearest function test')
    call mapping#space('nmap', ['t', 'l'], ':call dev#executerunner("testlast")<CR>', 'Run last tests')
    call mapping#space('nmap', ['t', 'c'], ':call dev#executerunner("coverage")<CR>', 'Toggle test coverage')

    call mapping#space("nmap", ['a', 'q'], ':call dev#executerunner("quickfix")', 'Quick fix')
    call mapping#space('nmap', ['a', 'f'], ':call dev#executerunner("codeformat")<CR>', 'Code format')
    call mapping#space('nmap', ['a', 'i'], ':call dev#executerunner("fiximports")<CR>', 'Fix imports/using')
    call mapping#space('nmap', ['a', 'a'], ':call dev#executerunner("viewcodeactions")<CR>', 'View code actions')
    call mapping#space("nmap", ['a', '.'], ':CocAction<CR>', 'Action list')
    call mapping#space("nmap", ['a', 's'], '<Plug>(coc-codeaction-selected)', 'Code action on selection')
    call mapping#space("nmap", ['a', 'b'], '<Plug>(coc-codeaction)', 'Code action on line')

    call mapping#space("nmap", ['G', 'd'], ':call dev#executerunner("definition")<CR>', 'Go to definition')
    call mapping#space("nmap", ['G', 'D'], ':vsp<CR>:call dev#executerunner("definition")<CR>', 'Go to definition (v-split)')
    call mapping#space("nmap", ['G', 't'], ':call dev#executerunner("typedefinition")<CR>', 'Go to type definition')
    call mapping#space("nmap", ['G', 'i'], ':call dev#executerunner("findimplementations")<CR>', 'Go to implementation')
    call mapping#space("nmap", ['G', 'r'], ':call dev#executerunner("findusages")<CR>', 'Go to references')

    call mapping#leader('vmap', ['a'], '<Plug>(coc-codeaction-selected)', 'Code Action for selection')
    call mapping#leader('nmap', ['a'], '<Plug>(coc-codeaction-selected)', 'Code Action for selection')
    call mapping#leader("nmap", ['t'], ':call dev#executerunner("test")<CR>', 'Run tests')
    call mapping#leader('nmap', ['r','n'], ':call dev#executerunner("rename")<CR>', 'Rename')
    call mapping#leader('nmap', ['r','b'], ':call dev#executerunner("build")<CR>', 'Run Build command')

endfunction

"########################################################################
" CoC Setup
"########################################################################
function! s:cocSetup()

    augroup mygroup
        autocmd!
        " Setup formatexpr specified filetype(s).
        autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
        " Update signature help on jump placeholder
        autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
    augroup end

    call extend(g:coc_global_extensions, [
                \ "coc-snippets",
                \ "coc-actions",
                \ "coc-vetur"])

    " Highlight symbol under cursor on CursorHold
    autocmd CursorHold * silent call CocActionAsync('highlight')

endfunction


"########################################################################
" Ale Setup
"########################################################################
function! s:aleSetup()
    let g:ale_linters = {}
    let g:ale_disable_lsp = 1
    let g:ale_fixers = {
                \   '*': ['remove_trailing_lines', 'trim_whitespace'],
                \}
    let g:ale_sign_error = '⛔'
    let g:ale_sign_warning = ''
    let g:ale_set_loclist = 0
    let g:ale_set_quickfix = 1
endfunction


"########################################################################
" Autoformat Setup
"########################################################################
function! s:autoformatSetup()

endfunction

function! dev#show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    else
        call CocActionAsync('doHover')
    endif
endfunction

function! dev#executerunner(key) abort
    if has_key(g:spaced_dev_settings["Commands"][a:key], &filetype)
        exe g:spaced_dev_settings["Commands"][a:key][&filetype]
    else
        if has_key(g:spaced_dev_settings["Commands"][a:key], '*')
            exe g:spaced_dev_settings["Commands"][a:key]['*']
        else
            echo "Command '". a:key . "' not implemented for the filetype '".&filetype."'"
        endif
    endif
endfunction

function! dev#register_command(lang, command, runnercommand)
    call extend(g:spaced_dev_settings["Commands"][a:command], { a:lang : a:runnercommand , })
endfunction
