call setup#registerlayer('lang#go')

function! lang#go#plugins() abort

    call setup#plugin('fatih/vim-go', {'do': ':GoUpdateBinaries'})

endfunction

function! lang#go#settings() abort
    " Settings

    let g:go_list_type = "quickfix"
    let g:go_fmt_command = "goimports"
    let g:go_fmt_fail_silently = 1
    let g:go_def_mapping_enabled = 0
    let g:go_doc_popup_window = 1
    let g:go_highlight_types = 1
    let g:go_highlight_fields = 1
    let g:go_highlight_functions = 1
    let g:go_highlight_methods = 1
    let g:go_highlight_operators = 1
    let g:go_highlight_build_constraints = 1
    let g:go_highlight_structs = 1
    let g:go_highlight_generate_tags = 1
    let g:go_highlight_space_tab_error = 0
    let g:go_highlight_array_whitespace_error = 0
    let g:go_highlight_trailing_whitespace_error = 0
    let g:go_highlight_extra_types = 1

    :call extend(g:ale_linters, { "go": ['golint', 'go vet'], })
    :call extend(g:coc_global_extensions, [ "coc-go" ])
    call dev#register_command("go", "test", ":GoTest ./...")
    call dev#register_command('go', 'testsuite', ':GoTest ./...')
    call dev#register_command('go', 'testfile', ':GoTest')
    call dev#register_command("go", "testfunc", ":GoTestFunc")
    call dev#register_command('go', 'testlast', ':GoTest ./...')
    call dev#register_command("go", "build", ":call lang#go#build_go_files()")
    call dev#register_command("go", "coverage", ":GoCoverageToggle")
    call dev#register_command("go", "debugstart", ":GoDebugStart")
    call dev#register_command("go", "debugbreakpoint", ":GoDebugBreakpoint")
    call dev#register_command("go", "alternate", ":GoAlternate")
    call dev#register_command("go", "viewdocumentation", ":GoDocBrowser")
    call dev#register_command("go", "codeformat", ":GoFmt")
    call dev#register_command("go", "fiximports", ":GoImports")
    call dev#register_command("go", "globalcodecheck", ":GoMetaLinter!")

    autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4

    augroup completion_preview_close
        autocmd!
        if v:version > 703 || v:version == 703 && has('patch598')
            autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
        endif
    augroup END

    " Keybindings

    let g:go_def_mapping_enabled = 0
    augroup go
        au!
        au Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
        au Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
        au Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
        au Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')

        au FileType go call mapping#spacemenu(['l', 'g'], 'Golang')
        au FileType go call mapping#space("nmap", ['l', 'g', 'r'], '<Plug>(go-run)', 'Run')
        au FileType go call mapping#space("nmap", ['l', 'g', 'f'], '<Plug>(go-info)', 'Info')
        au FileType go call mapping#space("nmap", ['l', 'g', 'i'], ':GoImpl<CR>', 'Implement Interface')
        au FileType go call mapping#space("nmap", ['l', 'g', 's'], ':GoFillStruct<CR>', 'Fill Struct')

        au FileType go call mapping#space("nmap", ['l', 'g', 'l'], '<Plug>(go-metalinter)', 'Metalinter')
        au FileType go call mapping#space("nmap", ['l', 'g', 'd'], ':GoDecls<CR>', 'Declarations in file')
        au FileType go call mapping#space("imap", ['l', 'g', 'd'], '<Esc>:<C-u>GoDecls<CR>', 'Declarations in file')
        au FileType go call mapping#space("nmap", ['l', 'g', 'p'], ':GoDeclsDir<CR>', 'Declarations in path')
        au FileType go call mapping#space("imap", ['l', 'g', 'p'], '<Esc>:<C-u>GoDeclsDir<CR>', 'Declarations in path')
    augroup END
endfunction


" run :GoBuild or :GoTestCompile based on the go file
function! lang#go#build_go_files()
    let l:file = expand('%')
    if l:file =~# '^\f\+_test\.go$'
        call go#test#Test(0, 1)
    elseif l:file =~# '^\f\+\.go$'
        call go#cmd#Build(0)
    endif
endfunction


