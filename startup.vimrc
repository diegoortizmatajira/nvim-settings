source $HOME/.config/nvim/nvim-settings/settings-general.vim
source $HOME/.config/nvim/nvim-settings/setup.vim
"------------------------------------------------
"SOURCE THE LAYERS HERE
"------------------------------------------------
source $HOME/.config/nvim/nvim-settings/layers/core.vim
source $HOME/.config/nvim/nvim-settings/layers/mapping.vim
source $HOME/.config/nvim/nvim-settings/layers/motions.vim
source $HOME/.config/nvim/nvim-settings/layers/fzf.vim
source $HOME/.config/nvim/nvim-settings/layers/dev.vim
source $HOME/.config/nvim/nvim-settings/layers/git.vim
source $HOME/.config/nvim/nvim-settings/layers/lang/javascript.vim
source $HOME/.config/nvim/nvim-settings/layers/lang/html.vim
source $HOME/.config/nvim/nvim-settings/layers/lang/go.vim
source $HOME/.config/nvim/nvim-settings/layers/lang/csharp.vim
source $HOME/.config/nvim/nvim-settings/layers/lang/vue.vim
source $HOME/.config/nvim/nvim-settings/layers/lang/vim.vim
source $HOME/.config/nvim/nvim-settings/layers/ui.vim

call setup#plugins()
call setup#layersettings()

