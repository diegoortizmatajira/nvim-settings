call setup#registerlayer('ui')

function! ui#plugins() abort

  call setup#plugin('vim-airline/vim-airline')
  call setup#plugin('vim-airline/vim-airline-themes')

  call setup#plugin('tomasr/molokai')
  call setup#plugin('morhetz/gruvbox')

  call setup#plugin('mhinz/vim-startify')
  call setup#plugin('junegunn/vim-peekaboo')
  call setup#plugin('Yggdroot/indentLine')
  call setup#plugin('nathanaelkane/vim-indent-guides')
  call setup#plugin('takac/vim-hardtime')

  " Should be the last plugin
  call setup#plugin('ryanoasis/vim-devicons')

endfunction

function! ui#settings() abort
  " Settings
  call s:airlineSetup()
  call s:indentLineSetup()
  call s:deviconsSetup()
  call s:startifySetup()

  " Set the default theme
  colorscheme gruvbox
  let g:airline_theme = 'gruvbox'
  let g:gruvbox_contrast_dark = 'hard'

  " Enables hard mode
  let g:hardtime_default_on = 1

  " Keybindings

  call mapping#space("noremap", ['b', '.'], ':Startify<CR>', 'Homepage')
  call mapping#space("noremap", ['A', '.'], ':Startify<CR>', 'Homepage')

  call mapping#space("nmap", ['u', 'i'], ':IndentGuidesToggle<CR>', 'Toggle indent guides')
  call mapping#space("nmap", ['u', 'h'], ':HardTimeToggle<CR>', 'Toggle HardTime key mode')

endfunction

"########################################################################
" Airline Setup
"########################################################################
function! s:airlineSetup()
  " vim-airline
  let g:airline#extensions#branch#enabled = 1
  let g:airline#extensions#ale#enabled = 1
  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tagbar#enabled = 1
  let g:airline_skip_empty_sections = 1

  " vim-airline
  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

  if !exists('g:airline_powerline_fonts')
    let g:airline#extensions#tabline#left_sep = ''
    let g:airline#extensions#tabline#left_alt_sep = '|'
    let g:airline_left_sep          = ''
    let g:airline_left_alt_sep      = '»'
    let g:airline_right_sep         = ''
    let g:airline_right_alt_sep     = '«'
    let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
    let g:airline#extensions#readonly#symbol   = '⊘'
    let g:airline#extensions#linecolumn#prefix = '¶'
    let g:airline#extensions#paste#symbol      = 'ρ'
    let g:airline_symbols.linenr    = '␊'
    let g:airline_symbols.branch    = '⎇'
    let g:airline_symbols.paste     = 'ρ'
    let g:airline_symbols.paste     = 'Þ'
    let g:airline_symbols.paste     = '∥'
    let g:airline_symbols.whitespace = 'Ξ'
  else
    let g:airline#extensions#tabline#left_sep = ''
    let g:airline#extensions#tabline#left_alt_sep = ''

    " powerline symbols
    let g:airline_left_sep = ''
    let g:airline_left_alt_sep = ''
    let g:airline_right_sep = ''
    let g:airline_right_alt_sep = ''
    let g:airline_symbols.branch = ''
    let g:airline_symbols.readonly = ''
    let g:airline_symbols.linenr = ''
  endif

endfunction

"########################################################################
" IndentLine Setup
"########################################################################
function! s:indentLineSetup()
  let g:indent_guides_default_mapping = 0
  let g:indentLine_enabled = 1
  let g:indentLine_concealcursor = 0
  let g:indentLine_char = '┊'
  let g:indentLine_faster = 1
endfunction


"########################################################################
" IndentLine Setup
"########################################################################
function! s:deviconsSetup()
  " vim-devicons
  let g:webdevicons_enable = 1
  let g:webdevicons_enable_nerdtree = 1
  let g:webdevicons_enable_unite = 1
  let g:webdevicons_enable_vimfiler = 1
  let g:webdevicons_enable_airline_tabline = 1
  let g:webdevicons_enable_airline_statusline = 1
  let g:webdevicons_enable_ctrlp = 1
  let g:webdevicons_enable_flagship_statusline = 1
  let g:WebDevIconsUnicodeDecorateFileNodes = 1
  let g:WebDevIconsUnicodeGlyphDoubleWidth = 1
  let g:webdevicons_conceal_nerdtree_brackets = 1
  let g:WebDevIconsNerdTreeAfterGlyphPadding = '  '
  let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1
  let g:webdevicons_enable_denite = 1
  let g:WebDevIconsUnicodeDecorateFolderNodes = 1
  let g:DevIconsEnableFoldersOpenClose = 1
  let g:DevIconsEnableFolderPatternMatching = 1
  let g:DevIconsEnableFolderExtensionPatternMatching = 1
  let WebDevIconsUnicodeDecorateFolderNodesExactMatches = 1
endfunction

"########################################################################
" Startify Setup
"########################################################################
function! s:startifySetup()
  " Startify settings
  let g:startify_session_dir = '~/.config/nvim/session'
  let g:startify_lists = [
        \ { 'type': 'sessions',  'header': ['   Sessions']       },
        \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
        \ { 'type': 'files',     'header': ['   Files']            },
        \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
        \ ]
  let g:startify_session_autoload = 1
  let g:startify_session_delete_buffers = 1
  let g:startify_custom_header = [
        \ '   ___       __    ______                            ',
        \ '   __ |     / /_______  /__________________ ________ ',
        \ '   __ | /| / /_  _ \_  /_  ___/  __ \_  __ `__ \  _ \',
        \ '   __ |/ |/ / /  __/  / / /__ / /_/ /  / / / / /  __/',
        \ '   ____/|__/  \___//_/  \___/ \____//_/ /_/ /_/\___/ ',
        \]
  let g:startify_change_to_dir = 0
  let g:startify_change_to_vcs_root = 1
endfunction


