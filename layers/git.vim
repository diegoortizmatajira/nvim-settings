call setup#registerlayer("git")

function! git#plugins() abort

    call setup#plugin('tpope/vim-fugitive')
    call setup#plugin('tpope/vim-rhubarb')
    call setup#plugin('shumphrey/fugitive-gitlab.vim')
    call setup#plugin('airblade/vim-gitgutter')
    call setup#plugin('tpope/vim-dispatch')

endfunction

function! git#settings() abort
    " Settings
    let g:gitgutter_map_keys = 0

    " Keybindings

    call mapping#space("noremap", ['g', 's'], ':Gstatus<CR>', 'Status')
    call mapping#space("noremap", ['g', 'S'], ':Git add %<CR>', 'Stage current file')
    call mapping#space("noremap", ['g', 'U'], ':Git reset -q %<CR>', 'Unstage current file')
    call mapping#space("noremap", ['g', 'c'], ':Gcommit<CR>', 'Commit changes')
    call mapping#space("noremap", ['g', 'a'], ':Gwrite<CR>', 'Write')
    call mapping#space("noremap", ['g', 'p'], ':Gpush<CR>', 'Push')
    call mapping#space("noremap", ['g', 'P'], ':Gpull<CR>', 'Pull')
    call mapping#space("noremap", ['g', 'd'], ':Gvdiff<CR>', 'View diff')
    call mapping#space("noremap", ['g', 'A'], ':Git add .<CR>', 'Stage all')
    call mapping#space("noremap", ['g', 'b'], ':Gblame<CR>', 'View blame')
    call mapping#space("noremap", ['g', 'r'], ':Gremove<CR>', 'Remove')
    call mapping#space("noremap", ['g', 'V'], ':Glog -- %<CR>', 'Current file log')
    call mapping#space("noremap", ['g', 'v'], ':Glog --<CR>', 'File log')
    call mapping#space("noremap", ['g', 'o'], ':.Gbrowse<CR>', 'Browse line on web')

    call mapping#spacemenu(['g','f'], 'Gitflow')
    call mapping#space("nmap", ['g', 'f', 'i'], ':Git flow init<CR>', 'Init')
    call mapping#space("nmap", ['g', 'f', 'f'], ':Git flow feature start ', 'Feature start')
    call mapping#space("nmap", ['g', 'f', 'F'], ':Git flow feature finish <C-R>=git#flowbranch()<CR>', 'Feature finish')
    call mapping#space("nmap", ['g', 'f', 'r'], ':Git flow release start ', 'Release start')
    call mapping#space("nmap", ['g', 'f', 'R'], ':Git flow release finish <C-R>=git#flowbranch()<CR>', 'Release finish')
    call mapping#space("nmap", ['g', 'f', 'h'], ':Git flow hotfix start ', 'Hotfix start')
    call mapping#space("nmap", ['g', 'f', 'H'], ':Git flow hotfix finish <C-R>=git#flowbranch()<CR>', 'Hotfix finish')

endfunction

function! git#flowbranch()
    let parts = split(FugitiveHead(),'/')
    return parts[len(parts)-1]
endfunction
