call setup#registerlayer('lang#html')

function! lang#html#plugins() abort

  call setup#plugin('mattn/emmet-vim')
  call setup#plugin('hail2u/vim-css3-syntax')
  call setup#plugin('gko/vim-coloresque')
  call setup#plugin('tpope/vim-haml')
  call setup#plugin('mattn/emmet-vim')

endfunction

function! lang#html#settings() abort
  " Settings

  :call extend(g:coc_global_extensions, [
        \ "coc-css",
        \ "coc-emmet",
        \ "coc-html"])
  let g:user_emmet_leader_key='<C-Z>'

  autocmd Filetype html setlocal ts=2 sw=2 expandtab

  " Keybindings

endfunction


"########################################################################
" Support Setup
"########################################################################
function s:support()

endfunction
