let g:spaced_layers = []
let g:spaced_plugins = {}

"*****************************************************************************
"" Vim-PLug core
"*****************************************************************************
let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')

if !filereadable(vimplug_exists)
    if !executable("curl")
        echoerr "You have to install curl or first install vim-plug yourself!"
        execute "q!"
    endif
    echo "Installing Vim-Plug..."
    echo ""
    silent exec "!\curl -fLo " . vimplug_exists . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
    let g:not_finish_vimplug = "yes"

    autocmd VimEnter * PlugInstall
endif

function! setup#plugins() abort
    "------------------------------------------------
    " INCLUDE THE PLUGINS OR CALL THE LAYERS PLUGIN FUNCTION HERE
    "------------------------------------------------

    call plug#begin(expand('~/.config/nvim/plugged'))
    call setup#layerplugins()
    call plug#end()
endfunction

function! setup#registerlayer(layername)
    call extend(g:spaced_layers, [a:layername])
endfunction

function! setup#layerplugins() abort
    "Loads layer plugin settings
    for l in g:spaced_layers
        exe 'call '. l . '#plugins()'
    endfor

    "Loads the plugins using Vim-Plug
    for plugin in items(g:spaced_plugins)
       if type(plugin[1]) == 4
           Plug plugin[0], plugin[1]
       else
           Plug plugin[0]
       endif
    endfor
endfunction

function! setup#layersettings() abort
    for l in g:spaced_layers
        exe 'call '. l . '#settings()'
    endfor
endfunction

function! setup#plugin(pluginname,...)
    let params = ''
    if a:0 > 0
        let params = a:1
    endif
    let pluginname = tolower(a:pluginname)
    if !has_key(g:spaced_plugins, pluginname)
        let g:spaced_plugins[pluginname] = params
    endif
endfunction
