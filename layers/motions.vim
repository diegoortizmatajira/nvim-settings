call setup#registerlayer('motions')

function! motions#plugins() abort

  call setup#plugin('easymotion/vim-easymotion')

endfunction

function! motions#settings() abort
  " Settings

  let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

  " Keybindings

  map  / <Plug>(easymotion-sn)
  omap / <Plug>(easymotion-tn)
  map  n <Plug>(easymotion-next)
  map  N <Plug>(easymotion-prev)
  call mapping#space("map", ['m', 'l'], '<Plug>(easymotion-lineforward)', 'EasyMotion ➡')
  call mapping#space("map", ['m', 'j'], '<Plug>(easymotion-j)', 'EasyMotion ⬇')
  call mapping#space("map", ['m', 'k'], '<Plug>(easymotion-k)', 'EasyMotion ⬆')
  call mapping#space("map", ['m', 'h'], '<Plug>(easymotion-linebackward)', 'EasyMotion ⬅')
  call mapping#space("map", ['m', 'f'], '<Plug>(easymotion-s2)', 'Find 2 characters')

  call mapping#leader("map", ['l'], '<Plug>(easymotion-lineforward)', 'EasyMotion ➡')
  call mapping#leader("map", ['j'], '<Plug>(easymotion-j)', 'EasyMotion ⬇')
  call mapping#leader("map", ['k'], '<Plug>(easymotion-k)', 'EasyMotion ⬆')
  call mapping#leader("map", ['h'], '<Plug>(easymotion-linebackward)', 'EasyMotion ⬅')

endfunction


