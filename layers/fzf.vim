call setup#registerlayer('fzf')

function! fzf#plugins() abort

    call setup#plugin('junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' })
    call setup#plugin('junegunn/fzf.vim')

endfunction

function! fzf#settings() abort
    " Settings
    call s:fzfSetup()

    " Keybindings

    " fzf file fuzzy search that respects .gitignore
    " If in git directory, show only files that are committed, staged, or unstaged
    " else use regular :Files
    nnoremap <expr> <C-p> (len(system('git rev-parse')) ? ':Files' : ':GFiles --exclude-standard --others --cached')."\<cr>"

    call mapping#space("nmap", ['b', '.'], ':Buffers<CR>', 'Buffer list')

    call mapping#space("noremap", ['f', '.'], ':Files<CR>', 'Find files')

    call mapping#space("noremap", ['s', '.'], ':Files<CR>', 'Files')
    call mapping#space("noremap", ['s', 'b'], ':Buffers<CR>', 'Buffers')
    call mapping#space("noremap", ['s', 'g'], ':GFiles<CR>', 'Git Files')
    call mapping#space("noremap", ['s', 'h'], ':History<CR>', 'Recent files')
    call mapping#space("noremap", ['s', 'c'], ':History:<CR>', 'Recent comands')
    call mapping#space("noremap", ['s', 'm'], ':Maps<CR>', 'Key Maps')
    call mapping#space("noremap", ['s', 'l'], ':BLines<CR>', 'Lines')
    call mapping#space("noremap", ['s', 't'], ':BTags<CR>', 'Tags')
    call mapping#space("noremap", ['s', 'a'], ':Ag<CR>', 'The Silver Searcher')
    call mapping#space("noremap", ['s', 'r'], ':Rg<CR>', 'Ripgrep')
    call mapping#space("noremap", ['s', 'f'], ':Filetypes<CR>', 'Filetypes')
    call mapping#space("noremap", ['s', 'o'], ':Colors<CR>', 'Color Schemes')
    call mapping#space("noremap", ['s', 'w'], ':Windows<CR>', 'Windows')
    call mapping#space("noremap", ['s', 's'], ':Snippets<CR>', 'Snippets')
    call mapping#space("noremap", ['s', ','], ':Commands<CR>', 'Commands')

    call mapping#space("noremap", ['G', 'm'], ':GFiles?<CR>', 'View modified files')

    call mapping#leader("noremap", ['b'], ':Buffers<CR>', 'Buffer list')

endfunction

"########################################################################
" CoC Setup
"########################################################################
function! s:cocSetup()

    let g:coc_config_home = "~/.config/nvim/nvim-settings/"

    " Removes CoC Explorer if it is the last buffer
    autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif

    " Use `:Format` to format current buffer
    command! -nargs=0 Format :call CocAction('format')

    " Use `:Fold` to fold current buffer
    command! -nargs=? Fold :call     CocAction('fold', <f-args>)

endfunction


"########################################################################
" FzF Setup
"########################################################################
function! s:fzfSetup()

    set wildmode=list:longest,list:full
    set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
    let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"

    "******* The Silver Searcher *********

    if executable('ag')
        let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
        set grepprg=ag\ --nogroup\ --nocolor
    endif

    "****** ripgrep ********

    if executable('rg')
        let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
        set grepprg=rg\ --vimgrep
        command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
    endif

endfunction


