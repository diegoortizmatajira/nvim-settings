call setup#registerlayer('lang#csharp')

function! lang#csharp#plugins() abort

  call setup#plugin('OmniSharp/omnisharp-vim')

endfunction

function! lang#csharp#settings() abort
  " Settings
  :call extend(g:ale_linters, { "cs": ['OmniSharp'], })
  :call extend(g:coc_global_extensions, [ "coc-omnisharp" ])
  call dev#register_command("cs", "test", ":OmniSharpRunTestsInFile")
  call dev#register_command("cs", "build", ":OmniSharpBuildAsync")
  call dev#register_command("cs", "viewdocumentation", ":OmniSharpDocumentation")


  " Keybindings

  augroup csharp
    au FileType cs call mapping#spacemenu(['l', 'c'], 'C Sharp')
    au Filetype cs call mapping#space("nmap", ['l', 'c', 't'], ':OmniSharpRunTest<CR>', 'Run test at cursor')
    au Filetype cs call mapping#space("nmap", ['l', 'c', 'T'], ':OmniSharpRunTestsInFile<CR>', 'Run test in file')
    au Filetype cs call mapping#space("nmap", ['l', 'c', 'S'], ':OmniSharpStartServer<CR>', 'Start Server')
    au Filetype cs call mapping#space("nmap", ['l', 'c', 's'], ':OmniSharpStopServer<CR>', 'Stop Server')
    au Filetype cs call mapping#space("nmap", ['l', 'c', 'r'], ':norm \<Plug>(omnisharp_restart_server)<CR>', 'Restart Server')

  augroup END

endfunction


"########################################################################
" Support Setup
"########################################################################
function s:support()

endfunction
