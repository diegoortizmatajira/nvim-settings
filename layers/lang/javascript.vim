call setup#registerlayer('lang#javascript')

function! lang#javascript#plugins() abort

  call setup#plugin('kevinoid/vim-jsonc')
  call setup#plugin('jelera/vim-javascript-syntax')
  call setup#plugin('leafgarland/typescript-vim')
  call setup#plugin('HerringtonDarkholme/yats.vim')

endfunction

function! lang#javascript#settings() abort
  " Settings
  let g:javascript_enable_domhtmlcss = 1
  let g:yats_host_keyword = 1
  :call extend(g:coc_global_extensions, [ 
    \ "coc-tsserver",
    \ "coc-eslint",
    \ "coc-json"])

  augroup vimrc-javascript
    autocmd!
    autocmd FileType javascript setl tabstop=4|setl shiftwidth=4|setl expandtab softtabstop=4
  augroup END

  " Keybindings

endfunction


