
call setup#registerlayer('core')

function! core#plugins() abort

    call setup#plugin('neoclide/coc.nvim', {'branch': 'release'})
    call setup#plugin('Shougo/vimproc.vim', {'do': 'make'})
    call setup#plugin('Raimondi/delimitMate')
    call setup#plugin('tpope/vim-commentary')
    call setup#plugin('tpope/vim-surround')
    call setup#plugin('vimwiki/vimwiki')

endfunction

function! core#settings() abort
    " Settings

    let g:coc_config_home = "~/.config/nvim/nvim-settings/"
    let g:coc_global_extensions = []

    call extend(g:coc_global_extensions, [
                \ "coc-explorer",
                \ "coc-yank"])

    " Removes CoC Explorer if it is the last buffer
    autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif

    " Keybindings

    "" CoC-Explorer
    nnoremap <silent> <F3> :CocCommand explorer --position floating<CR>

    "" Standard Commands
    nmap <C-s> :w<CR>
    imap <C-s> <Esc>:w<CR>
    "" Switching windows
    noremap <C-j> <C-w>j
    noremap <C-k> <C-w>k
    noremap <C-l> <C-w>l
    noremap <C-h> <C-w>h
    "*****************************************************************************
    "" Abbreviations
    "*****************************************************************************
    "" no one is really happy until you have this shortcuts
    cnoreabbrev W! w!
    cnoreabbrev Q! q!
    cnoreabbrev Qall! qall!
    cnoreabbrev Wq wq
    cnoreabbrev Wa wa
    cnoreabbrev wQ wq
    cnoreabbrev WQ wq
    cnoreabbrev W w
    cnoreabbrev Q q
    cnoreabbrev Qall qall

    " Use tab for trigger completion with characters ahead and navigate.
    " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
    " other plugin before putting this into your config.
    inoremap <silent><expr> <TAB>
                \ pumvisible() ? "\<C-n>" :
                \ <SID>check_back_space() ? "\<TAB>" :
                \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    " Use <c-space> to trigger completion.
    if has('nvim')
        inoremap <silent><expr> <c-space> coc#refresh()
    else
        inoremap <silent><expr> <c-@> coc#refresh()
    endif

    " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
    " Coc only does snippet and additional edit on confirm.
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

    call mapping#spacemenu(['g'], 'Git Repository')
    call mapping#spacemenu(['t'], 'Tests')
    call mapping#spacemenu(['a'], 'Actions')
    call mapping#spacemenu(['A'], 'Application')
    call mapping#spacemenu(['b'], 'Buffers')
    call mapping#spacemenu(['d'], 'Development')
    call mapping#spacemenu(['D'], 'Debug')
    call mapping#spacemenu(['e'], 'Edit')
    call mapping#spacemenu(['f'], 'Files')
    call mapping#spacemenu(['f', 'c'], 'Config')
    call mapping#spacemenu(['G'], 'Go to')
    call mapping#spacemenu(['l'], 'Language specified')
    call mapping#spacemenu(['m'], 'Motion')
    call mapping#spacemenu(['q'], 'Quit')
    call mapping#spacemenu(['s'], 'Search')
    call mapping#spacemenu(['u'], 'User interface')
    call mapping#spacemenu(['v'], 'View')

    call mapping#space("nmap", ['f', 'c', 'c'], ':CocConfig', 'Open CoC Config')

    call mapping#space("nmap", ['v', 'l'], ':cope<CR>', 'List all errors')
    call mapping#space("nmap", ['v', 'r'], ':registers<CR>', 'Registers')
    call mapping#space("nmap", ['v', 'y'], ':<C-u>CocList -A --normal yank<CR>', 'Yank list')

    call mapping#space("nmap", ['v', 'j'], ':<C-u>CocNext<CR>', 'Next item')
    call mapping#space("nmap", ['v', 'k'], ':<C-u>CocPrev<CR>', 'Previous item')
    call mapping#space("nmap", ['v', 'p'], ':<C-u>CocListResume<CR>', 'Resume list')
    call mapping#space("nmap", ['v', 'e'], ':<C-u>CocList extensions<CR>', 'Extensions')
    call mapping#space("nmap", ['s', 'v'], ':<C-u>CocList vimcommands<CR>', 'Vim Commands')

    call mapping#space("nmap", ['b', 'n'], ':bn<CR>', 'Next buffer')
    call mapping#space("nmap", ['b', 'p'], ':bp<CR>', 'Previous buffer')
    call mapping#space("nmap", ['b', 'd'], ':bd<CR>', 'Close buffer')
    call mapping#space("nmap", ['b', 'D'], ':bd!<CR>', 'Force close buffer')
    call mapping#space("nmap", ['b', 'x'], ':bufdo bd<CR>', 'Close all buffers')
    call mapping#space("nmap", ['b', 'X'], ':bufdo bd!<CR>', 'Force close all buffers')
    call mapping#space("nmap", ['b', 'h'], ':<C-u>split<CR>', 'Horizonal split')
    call mapping#space("nmap", ['b', 'v'], ':<C-u>vsplit<CR>', 'Vertical split')
    call mapping#space("nmap", ['b', 'w'], ':setl readonly!<CR>', 'Toggle read only mode')
    call mapping#space("nmap", ['b', 'Y'], ':normal! ggVG"+y``<CR>', 'Copy whole buffer to clipboard')
    call mapping#space("nmap", ['b', 'P'], ':normal! ggdG"+P<CR>', 'Paste clipboard to whole buffer')
    call mapping#space("nmap", ['b', 'n'], ':enew<CR>', 'New empty buffer')

    call mapping#space("nmap", ['f', 's'], ':w<CR>', 'Save current file')
    call mapping#space("nmap", ['f', 'S'], ':wall<CR>', 'Save all files')
    call mapping#space("nmap", ['f', 't'], ':CocCommand explorer<CR>', 'Toggle file tree')
    call mapping#space("nmap", ['f', 'e'], ':CocCommand explorer --position floating<CR>', 'Explore files')

    call mapping#space("nmap", ['A', 'i'], ':PlugInstall<CR>', 'Install plugins')
    call mapping#space("nmap", ['A', 'u'], ':PlugUpdate<CR>', 'Update plugins')
    call mapping#space("nmap", ['A', 'w'], ':VimwikiIndex<CR>', 'Wiki')
    call mapping#space("nmap", ['A', 't'], ':terminal<CR>', 'Terminal')

    call mapping#space("nmap", ['q', '.'], ':q<CR>', 'Close window')
    call mapping#space("nmap", ['q', 'q'], ':qa<CR>', 'Quit')
    call mapping#space("nmap", ['q', 'Q'], ':qa!<CR>', 'Force Quit')

    call mapping#space("nmap", ['e', 'u'], 'u', 'Undo')
    call mapping#space("nmap", ['e', 'r'], '<C-r>', 'Redo')

    call mapping#space("nmap", ['u', 'w'], ':setlocal wrap!<CR>', 'Toggle line wrap')

    call mapping#leader("nnoremap", ['<Space>'], ':noh<CR>', 'Clear search highlights')

    call mapping#leader("noremap", ['z'], ':bp<CR>', 'Previous buffer')
    call mapping#leader("noremap", ['x'], ':bn<CR>', 'Next buffer')
    call mapping#leader("noremap", ['c'], ':bd<CR>', 'Close buffer')
    call mapping#leader("noremap", ['q'], ':q<CR>', 'Close window')
    call mapping#leader("noremap", ['h'], ':<C-u>split<CR>', 'Horizonal split')
    call mapping#leader("noremap", ['v'], ':<C-u>vsplit<CR>', 'Vertical split')

endfunction

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

