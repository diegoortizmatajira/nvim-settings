call setup#registerlayer('lang#javascript')

function! lang#javascript#plugins() abort


endfunction

function! lang#javascript#settings() abort
  " Settings

  " call dev#register_command('javascript', 'test', '')
  " call dev#register_command('javascript', 'testsuite', '')
  " call dev#register_command('javascript', 'testfile', '')
  " call dev#register_command('javascript', 'testfunc', '')
  " call dev#register_command('javascript', 'testlast', '')
  " call dev#register_command('javascript', 'build', '')
  " call dev#register_command('javascript', 'coverage', '')
  " call dev#register_command('javascript', 'debugstart', '')
  " call dev#register_command('javascript', 'debugbreakpoint', '')
  " call dev#register_command('javascript', 'alternate', '')
  " call dev#register_command('javascript', 'findusages', '')
  " call dev#register_command('javascript', 'findimplementations', '')
  " call dev#register_command('javascript', 'refactor', '')
  " call dev#register_command('javascript', 'previewdefinitions', '')
  " call dev#register_command('javascript', 'previewimplementations', '')
  " call dev#register_command('javascript', 'typelookup', '')
  " call dev#register_command('javascript', 'viewdocumentation', '')
  " call dev#register_command('javascript', 'findsymbol', '')
  " call dev#register_command('javascript', 'fiximports', '')
  " call dev#register_command('javascript', 'signaturehelp', '')
  " call dev#register_command('javascript', 'globalcodecheck', '')
  " call dev#register_command('javascript', 'viewcodeactions', '')
  " call dev#register_command('javascript', 'codeformat', '')
  " call dev#register_command('javascript', 'rename', '')
  " call dev#register_command('javascript', 'definition', '')
  " call dev#register_command('javascript', 'typedefinition', '')
  " call dev#register_command('javascript', 'quickfix', '')

  " Keybindings

endfunction


"########################################################################
" Support Setup
"########################################################################
function s:support()

endfunction
