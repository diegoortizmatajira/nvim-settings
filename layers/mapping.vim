call setup#registerlayer('mapping')

let g:space_keymap={}
let g:leader_keymap={}
let g:spaced_mappings_enabled = 1

let g:spaced_mappings_settings = {
            \'MappingUIEnabled': 1,
            \'LeaderKeyMap': { 'name': 'Leader key commands' },
            \'SpaceKeyMap': { 'name': 'Space key commands' }
            \}

function! mapping#plugins() abort

    if g:spaced_mappings_settings['MappingUIEnabled']
        call setup#plugin('liuchengxu/vim-which-key')
    endif

endfunction

function! mapping#settings() abort
    " Settings
    if g:spaced_mappings_settings['MappingUIEnabled']
        call which_key#register('<Space>', g:spaced_mappings_settings['SpaceKeyMap'])
        call which_key#register(',', g:spaced_mappings_settings['LeaderKeyMap'])

        let g:which_key_timeout = 100

        " Keybindings
        nnoremap <silent> <Leader> :WhichKey '<Leader>'<CR>
        nnoremap <silent> <Space> :WhichKey '<Space>'<CR>
    endif

endfunction

function! mapping#leader(m, keys, cmd, desc) abort
    exe a:m . ' <silent> <Leader>' . join(a:keys, '') . ' ' . a:cmd
    call s:recursive_doc(g:spaced_mappings_settings['LeaderKeyMap'], a:keys, a:desc, 0)
endfunction

function! mapping#space(m, keys, cmd, desc) abort
    exe a:m . ' <silent> <Space>' . join(a:keys, '') . ' ' . a:cmd
    call s:recursive_doc(g:spaced_mappings_settings['SpaceKeyMap'], a:keys, a:desc, 0)
endfunction

function! mapping#spacemenu(keys, desc)
    call s:recursive_doc(g:spaced_mappings_settings['SpaceKeyMap'], a:keys, { 'name': '[ '. a:desc .' ]'}, 0)
endfunction

function! s:recursive_doc(list, keys, desc, i) abort
    if g:spaced_mappings_settings['MappingUIEnabled']
        if a:i == len(a:keys)-1
            let a:list[a:keys[a:i]] = a:desc
        else
            if !has_key(a:list, a:keys[a:i])
                let a:list[a:keys[a:i]]={}
            endif
            if type(a:list[a:keys[a:i]]) != 4
                " The next iteration would produce and error as the next node is not a
                " dictionary
                let pendingkeys = a:keys[a:i:-1]
                let collapsedkeys = a:keys[0:a:i]
                let collapsedkeys[a:i] = join(pendingkeys,'')
                call s:recursive_doc(a:list,collapsedkeys,a:desc, a:i)
                return
            else
                call s:recursive_doc(a:list[a:keys[a:i]], a:keys, a:desc, a:i+1)
            endif
        endif
    endif
endfunction
