call setup#registerlayer('lang#vim')

function! lang#vim#plugins() abort


endfunction

function! lang#vim#settings() abort
  " Settings

  call extend(g:ale_linters, { 'vim': ['vint'], })
  call extend(g:coc_global_extensions, [ 'coc-vimlsp' ])

  " dev#register_command('vim', 'test', '')
  " dev#register_command('vim', 'testfunc', '')
  " dev#register_command('vim', 'build', '')
  " dev#register_command('vim', 'coverage', '')
  " dev#register_command('vim', 'debugstart', '')
  " dev#register_command('vim', 'debugbreakpoint', '')
  " dev#register_command('vim', 'alternate', '')
  " dev#register_command('vim', 'findusages', '')
  " dev#register_command('vim', 'findimplementations', '')
  " dev#register_command('vim', 'refactor', '')
  " dev#register_command('vim', 'previewdefinitions', '')
  " dev#register_command('vim', 'previewimplementations', '')
  " dev#register_command('vim', 'typelookup', '')
  " dev#register_command('vim', 'viewdocumentation', '')
  " dev#register_command('vim', 'findsymbol', '')
  " dev#register_command('vim', 'fiximports', '')
  " dev#register_command('vim', 'signaturehelp', '')
  " dev#register_command('vim', 'globalcodecheck', '')
  " dev#register_command('vim', 'viewcodeactions', '')
  " dev#register_command('vim', 'codeformat', '')
  " dev#register_command('vim', 'rename', '')
  " dev#register_command('vim', 'definition', '')
  " dev#register_command('vim', 'typedefinition', '')
  " dev#register_command('vim', 'quickfix', '')

  " Keybindings

endfunction


"########################################################################
" Support Setup
"########################################################################
function s:support()

endfunction
